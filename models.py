from sqlalchemy import Column, Integer, String
from database import Base
import json

def to_json(instance, cls):
    replace = dict()    
    data = dict()
    for col in cls.__table__.columns:
        value = getattr(instance, col.name)
        if col.type in replace.keys() and value is not None:
            try:
                data[col.name] = replace[col.type](value)
            except:
                data[col.name] = "Error:  Failed to covert using ", str(replace[col.type])
        elif value is None:
            data[col.name] = str()
        else:
            data[col.name] = value
    return json.dumps(data)

class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    name = Column(String(50), unique=True)

    def __init__(self, name=None):
        self.name = name        

    def __repr__(self):
        return '<User %r>' % (self.name)
    @property
    def json(self):
        return to_json(self, self.__class__)