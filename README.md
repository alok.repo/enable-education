# EnableEducation

A simple REST endpoint demonstrating CRUD functionality using Flask and SQLAlchemy.

# Prerequisites
Python 3.x on Windows

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for testing purposes. 

### Installing

1. Download repo and cd enableeducation\scripts and run activate.bat to activate venv
2. pip install -r requirements.txt
3. python -m flask run

## Test Endpoints

1. GET http://localhost:5000/users - Returns all users
2. POST http://localhost:5000/users - Create new user
3. GET http://localhost:5000/user/1 - To get single user
4. PUT http://localhost:5000/user/1 - To Update user with id:1
5. DELETE http://localhost:5000/user/1 - To delete user with id:1

## PostMan screenshots

![AllUsers](docs/postman_allusers_json.png)
![CreateUser](docs/postman_create_user.png)
![DeleteUser](docs/postman_user_deleted.png)
![GetUser](docs/postman_get_user_json.png)
![Database](docs/database.png)


## License

This project is licensed under the MIT License.

