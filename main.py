from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from database import db_session
from models import User
import logging
app = Flask(__name__)

@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler('enable-main.log')
fh.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)

@app.route('/users', methods=['GET', 'POST'])
def get_users():
    if request.method == 'GET':
        all_users = User.query.all()
        users = [x.json for x in all_users]        
        return jsonify(users)
    data = request.get_json()
    logger.debug("Received : {0}".format(data))
    if not data:
        return 'Unsupported MimeType requested. Only application/json type is supported'

    if 'name' not in data.keys():
        return 'Invalid request, expecting a {"name" : "newusername"} request type'    
    new_user = User('{0}'.format(data['name']))
    db_session.add(new_user)
    db_session.commit()
    return new_user.json    

@app.route('/user/<int:id>', methods=['GET', 'PUT', 'DELETE'])
def get_update_user(id):
    if request.method == 'GET':                
        user = User.query.get(id)
        if user:
            return user.json
        return 'Invalid user'
    
    data = request.get_json()
    logger.debug("Received : {0}".format(data))
    if not data:
        return 'Unsupported MimeType requested. Only application/json type is supported'

    if 'name' not in data.keys():
        return 'Invalid request, expecting a {"name" : "newusername"} request type'

    if request.method == 'PUT':
        row = User.query.get(id)
        row.name = '{0}'.format(data['name'])
        db_session.commit()        
        return 'Record {0} updated successfully'.format(id)    
    elif request.method == 'DELETE':
        row = User.query.get(id)
        db_session.delete(row)
        db_session.commit()                
        return 'Record {0} deleted successfully'.format(id)       
